﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeBarHeirarchy : MonoBehaviour {

    private GameMeta meta;

	// Use this for initialization
	void Start () {
        meta = GameObject.Find("GameMeta").GetComponent<GameMeta>();
	}
	
	// Update is called once per frame
	void Update () {
		for(int i = 0; i < 4; i++)
        {
            if(!meta.playset[i].isPlaying)
            {
                GameObject.Find("Canvas/ChargeBarHeirarchy/Player" + (i + 1) + "Bar").SetActive(false);

            }
        }
	}
}
