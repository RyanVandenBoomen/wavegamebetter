﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimHand : MonoBehaviour {
    Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void TrueMove()
    {
        anim.SetBool("Move", true);
    }

    public void FalseMove()
    {
        anim.SetBool("Move", false);
    }

}
