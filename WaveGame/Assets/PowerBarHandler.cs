﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerBarHandler : MonoBehaviour {
    public float scaleEmpty;
    public float scaleFull;
    public float ratioEmpty;
    public float ratioFull;
    public string playerInstance;
    public RectTransform rect;
    private PlayerController player;
    public float ratio;

	// Use this for initialization
	void Start () {
        rect = GetComponent<RectTransform>();

    }
	
	// Update is called once per frame
	void Update () {
        player = GameObject.Find(playerInstance).GetComponent<PlayerController>();
        ratio = (player.chargeRatio - ratioEmpty) / (ratioFull - ratioEmpty);
        rect.sizeDelta = new Vector3(ratio * scaleFull, rect.sizeDelta.y);
    }
}
