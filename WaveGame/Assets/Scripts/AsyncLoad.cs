﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//written by Ryan Vanden Boomen
//
//script that seamlessly loads gameplay objects into game after menu

public class AsyncLoad : MonoBehaviour {

    public Scene gameplay;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    //destroys camera and loads gameplay scene additively
    public void LoadGameplay()
    {
        GameObject camera = GameObject.Find("GameCamera");
        Destroy(camera);
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
        Destroy(GameObject.Find("Menu"));
        Destroy(this.gameObject);
    }
}
