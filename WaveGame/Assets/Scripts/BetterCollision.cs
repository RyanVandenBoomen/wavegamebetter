﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterCollision : MonoBehaviour {
    public bool fast = true;
    private Rigidbody rigid;
	// Use this for initialization
	void Start () {
        rigid = GetComponent<Rigidbody>();
        if (fast)
        {
            rigid.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        }
        else
        {
            rigid.collisionDetectionMode = CollisionDetectionMode.Continuous;
        }

    }
}
