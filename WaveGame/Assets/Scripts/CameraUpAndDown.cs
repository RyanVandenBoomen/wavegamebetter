﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraUpAndDown : MonoBehaviour {

    public Transform top;
    public Transform bot;
    public string startingPosition;
    public float smoothTime = 0.3F;
    private string position;
    private Vector3 velocity = Vector3.zero;

    void Start()
    {
        position = startingPosition;
        if (position == "top")
        {
            transform.position = top.TransformPoint(new Vector3(0, 5, -10));
        }
        else if (position == "bottom")
        {
            transform.position = bot.TransformPoint(new Vector3(0, 5, -10));
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Switch();
            Debug.Log(position);
        }
        if (position == "top")
        {
            Vector3 targetPosition = top.TransformPoint(new Vector3(0, 5, -10));
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        } else if(position == "bot")
        {
            Vector3 targetPosition = bot.TransformPoint(new Vector3(0, 5, -10));
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        }
    }

    void Switch()
    {
        if(position == "top")
        {
            position = "bot";
        }
        else if (position == "bot")
        {
            position = "top";
        }
        else
        {
            Debug.Log("False position");
        }
    }
}
