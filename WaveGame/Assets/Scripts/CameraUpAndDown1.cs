﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraUpAndDown1 : MonoBehaviour {

    public Transform top;
    public Transform bot;
    public string startingPosition;
    public float smoothTime = 0.3F;
    private string position;
    private Vector3 velocity = Vector3.zero;

    void Start()
    {
        position = startingPosition;
        if (position == "top")
        {
            transform.position = top.transform.position;
        }
        else if (position == "bottom")
        {
            transform.position = bot.transform.position;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Switch();
            Debug.Log(position);
        }
        if (position == "top")
        {
            Vector3 targetPosition = top.transform.position;
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        } else if(position == "bot")
        {
            Vector3 targetPosition = bot.transform.position;
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        }
    }

    public void Switch()
    {
        if(position == "top")
        {
            position = "bot";
        }
        else if (position == "bot")
        {
            position = "top";
        }
        else
        {
            Debug.Log("False position");
        }
    }
}
