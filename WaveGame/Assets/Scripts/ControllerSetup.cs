﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

//written by Ryan Vanden Boomen
//
//Sets up what players are playing and runs backend of player select screen

public class ControllerSetup : MonoBehaviour {

    public GameMeta meta;
    public AsyncLoad load;
    public bool readyToPlay = false;

	// Use this for initialization
	void Start () {
        load = GameObject.Find("AsyncLoad").GetComponent<AsyncLoad>();
        meta = GameObject.Find("GameMeta").GetComponent<GameMeta>();
	}
	
	// Update is called once per frame
	void Update () {
        CheckControllers();
        MoveToNextRoom();
	}

    void CheckControllers()
    {
        if(Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            meta.playset[0].isPlaying = true;
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            meta.playset[0].isPlaying = false;
        }
        if (Input.GetKeyDown(KeyCode.Joystick2Button0))
        {
            meta.playset[1].isPlaying = true;
        }
        if (Input.GetKeyDown(KeyCode.Joystick2Button1))
        {
            meta.playset[1].isPlaying = false;
        }
        if (Input.GetKeyDown(KeyCode.Joystick3Button0))
        {
            meta.playset[2].isPlaying = true;
        }
        if (Input.GetKeyDown(KeyCode.Joystick3Button1))
        {
            meta.playset[2].isPlaying = false;
        }
        if (Input.GetKeyDown(KeyCode.Joystick4Button0))
        {
            meta.playset[3].isPlaying = true;
        }
        if (Input.GetKeyDown(KeyCode.Joystick4Button1))
        {
            meta.playset[3].isPlaying = false;
        }

    }

    void MoveToNextRoom()
    {
        int count = 0;
        for (int i = 0; i < meta.playset.Length; i++)
        {
            if(meta.playset[i].isPlaying)
            {
                count++;
            }
        }
        if (count > 1)
        {
            readyToPlay = true;
            if (Input.GetKeyDown(KeyCode.JoystickButton7))
            {
                load.LoadGameplay();
                Destroy(this.gameObject);
            }
        }
        else
        {
            readyToPlay = false;
        }
    }
}
