﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//written by Ryan Vanden Boomen
//
//Controls dodgeball switching mechanic
//Ensures ball is frozen when so

public class DodgeBallController : MonoBehaviour {
    public Material playerOneTexture;
    public Material playerTwoTexture;
    public Material playerThreeTexture;
    public Material playerFourTexture;
    public Material defaultTexture;
    public PlayerController.players currentPlayer;
    public float activeTime;
    public float ballCounter;
    private SpriteRenderer rend;

    public AudioSource audio;


    public bool freeze;

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
		currentPlayer = PlayerController.players.none;
        rend = GetComponentInChildren<SpriteRenderer>();
	}

    void FixedUpdate()
    {
        if (freeze)
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationZ;
        }
        else
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
        }
    }

    void Update()
    {
       
        CountBall();
    }
	
	void SwitchPlayer(PlayerController.players player)
    {
        currentPlayer = player;
        if(player == PlayerController.players.player1)
        {
            rend.material = playerOneTexture;
        }
        else if (player == PlayerController.players.player2)
        {
            rend.material = playerTwoTexture;
        }
        else if (player == PlayerController.players.player3)
        {
            rend.material = playerThreeTexture;
        }
        else if (player == PlayerController.players.player4)
        {
            rend.material = playerFourTexture;
        }
        else
        {
            rend.material = defaultTexture;
            Debug.Log("Nonvalid player in DodgeBallController");
        }
    }

    public void ActivateBall(PlayerController.players player)
    {
        SwitchPlayer(player);
        ballCounter = 0;
    }

    void CountBall()
    {
        ballCounter += Time.deltaTime;
        if(ballCounter >= activeTime)
        {
            if(currentPlayer != PlayerController.players.none)
            {
                SwitchPlayer(PlayerController.players.none);
            }
        }
    }

    void OnCollisionEnter(Collision c)
    {
        audio.Play();
    }
    
        
}
