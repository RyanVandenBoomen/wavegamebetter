﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//written by Ryan Vanden Boomen
//
//Displays score at end of game

public class FinalTextDraw : MonoBehaviour {

    public GameMeta meta;
    public TextMesh text;
    public int index;

	// Use this for initialization
	void Start () {
        meta = GameObject.Find("GameMeta").GetComponent<GameMeta>();
        text = GetComponent<TextMesh>();
        text.text = "Game Over\n";
        if (meta.playset[0].isPlaying)
            text.text += "Green: " + meta.playset[0].score + " Frags\n";
        if (meta.playset[1].isPlaying)
            text.text += "Yellow: " + meta.playset[1].score + " Frags\n";
        if (meta.playset[2].isPlaying)
            text.text += "Pink: " + meta.playset[2].score + " Frags\n";
        if (meta.playset[3].isPlaying)
            text.text += "Blue: " + meta.playset[3].score + " Frags\n";
        text.text += "\nPress Start to Continue";
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FindMax()
    {
        int curMax = 0;
        for(int  i = 0; i > meta.playset.Length; i++)
        {
            if(meta.playset[i].score>curMax)
            {
                curMax = meta.playset[i].score;
                index = i;
            }
        }
    }
}
