﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//written by Ryan Vanden Boomen
//
//Contains player atributes that will persist through scenes

public class GameMeta : MonoBehaviour {

    public Player[] playset = new Player[4];
    public AudioSource audio;
    public AudioClip[] death;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }


    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        CheckForControllers();
	}

    public void KillSound()
    {
        audio.clip = death[Random.Range(0, 7)];
        audio.Play();
    }


    void CheckForControllers()
    {
        int index = 0;
        string[] controllers = Input.GetJoystickNames();
        for(int i = 0; i < controllers.Length && index < 4; i++)
        {
            if(controllers[i].Contains("Controller"))
            {
                playset[index].isConnected = true;
                index++;    
            }
            
        }
    }

    public void AddScore(PlayerController.players player)
    {
        for(int i = 0; i < playset.Length; i++)
        {
            if(playset[i].player == player)
            {
                playset[i].increaseScore();
            }
        }
    }
}
