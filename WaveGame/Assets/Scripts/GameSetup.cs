﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//written by Ryan Vanden Boomen
//
//Sets up gameplay as well as events throughout the gameplay phase

public class GameSetup : MonoBehaviour {

    public GameObject finalText;
    public Transform[] startSpawn;
    public GameMeta meta;
    public GameObject[] players;
    public DodgeBallController firstBall;
    public List<GameObject> playerObjects;
    public SpawnCollector ballSpawns;

    private int count = 0;
    public float freezeCount = 0;
    public float freezeTime;

    public bool timerActivated = false;
    public float timerStart;
    public bool gameOver = false;
    public bool reset = false;
    public TextMesh scoreboard;

    public float ballTimer = 0;
    public float ballInterval;

    // Use this for initialization
    void Start() {
        firstBall = GameObject.Find("Dodgeball").GetComponent<DodgeBallController>();
        firstBall.freeze = true;
        startSpawn = GameObject.Find("StartSpawn").GetComponentsInChildren<Transform>();
        meta = GameObject.Find("GameMeta").GetComponent<GameMeta>();
        scoreboard = GameObject.Find("Scoreboard").GetComponentInChildren<TextMesh>();
        ballSpawns = GameObject.FindGameObjectWithTag("Spawner").GetComponent<SpawnCollector>();
        InitializeGame();
    }

    // Update is called once per frame
    void Update() {
        if (!gameOver)
        {
            UnFreeze();
        }
        if(timerActivated)
        {
            CountDown();
            if(!gameOver)
            {
                BallCounter();
            }
            if(reset)
            {
                WaitForReset();
            }
        }
    }

    void WaitForReset()
    {
        if(Input.GetKeyDown(KeyCode.JoystickButton7))
        {
            Destroy(GameObject.Find("GameMeta"));
            SceneManager.LoadSceneAsync(0);
        }
    }

    void BallCounter()
    {
        ballTimer += Time.deltaTime;
        if(ballTimer > ballInterval)
        {
            ballTimer = 0;
            ballSpawns.spawners[Random.Range(0, 2)].SpawnBall();
        }
    }

    void CountDown()
    {
        timerStart -= Time.deltaTime;
        int curTime = Mathf.CeilToInt(timerStart);
        if (curTime >= 0)
        {
            scoreboard.text = curTime.ToString();
        }
        if(curTime <= 0 && !gameOver)
        {
            gameOver = true;
            Freeze();
        }
    }

    void Freeze()
    {
        GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
        GameObject[] playersA= GameObject.FindGameObjectsWithTag("Player");

        for(int i = 0; i < balls.Length; i++)
        {
            balls[i].GetComponent<DodgeBallController>().freeze = true;
        }
        for(int i = 0; i < playersA.Length; i++)
        {
            playersA[i].GetComponent<PlayerController>().rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY |RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            playersA[i].GetComponent<PlayerController>().freeze = true;
        }

        StartCoroutine(GameOver());
        
    }

    IEnumerator GameOver()
    {
        yield return new WaitForSeconds(3);
        MoveCamera camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MoveCamera>();
        Instantiate(finalText);
        camera.MoveReturn();
        yield return new WaitForSeconds(5);
        reset = true;
    }
    void UnFreeze()
    {
        freezeCount += Time.deltaTime;
        scoreboard.text = Mathf.CeilToInt(freezeTime-freezeCount).ToString();
        if (freezeCount >= freezeTime)
        {
            firstBall.freeze = false;
            for (int i = 0; i < playerObjects.Count; i++)
            {
                playerObjects[i].GetComponent<PlayerController>().freeze = false;
                
            }
            playerObjects.Clear();
            timerActivated = true;
        }
        
    }

    void InitializeGame()
    {
        for (int i = 0; i < 4; i++)
        {
            if(meta.playset[i].isPlaying)
            {
                playerObjects.Add(Instantiate(players[i], startSpawn[i+1].position, Quaternion.identity));
                playerObjects[count].GetComponent<PlayerController>().freeze = true;
                count++;
            }
        }
    }
    
}
