﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    enum options { Start, HowToPlay, Credits, Quit };

    public GameObject start;
    public GameObject howToPlay;
    public GameObject credits;
    public GameObject quit;
    public GameObject mainMenu;
    public GameObject howToPlayScreen;
    public GameObject creditsScreen;
    public GameObject playerScreen;
    public GameObject logo;
    public GameObject controllerSetup;
    
    public bool selectingPlayers;

    private options currentOption;
    private bool selected;
    private bool moved;

    // Use this for initialization
    void Start () {
        
        selected = false;
        selectingPlayers = false;
        currentOption = options.Start;
        start.GetComponent<MainMenuItem>().Switch();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (!selected)
        {
            if (Input.GetAxis("VerticalP") < 0.5 && Input.GetAxis("VerticalP") > -0.5 && moved)
            {
                moved = false;
            }
            if (Input.GetAxis("VerticalP") > 0.5 && !moved)
            {
                Switch(false);
                moved = true;
            }
            else if (Input.GetAxis("VerticalP") < -0.5 && !moved)
            {
                Switch(true);
                moved = true;
            }
            if (Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.Joystick2Button0) ||
                Input.GetKeyDown(KeyCode.Joystick3Button0) || Input.GetKeyDown(KeyCode.Joystick4Button0))
            {
                Select();
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Joystick2Button1) ||
                Input.GetKeyDown(KeyCode.Joystick3Button1) || Input.GetKeyDown(KeyCode.Joystick4Button1))
            {
                Unselect();
            }
        }
    }

    void Switch (bool up)
    {
        if (!selected)
        {
            if (up)
            {
                if (currentOption == options.Start)
                {
                    currentOption = options.Quit;
                    start.GetComponent<MainMenuItem>().Switch();
                    quit.GetComponent<MainMenuItem>().Switch();
                }
                else if (currentOption == options.HowToPlay)
                {
                    currentOption = options.Start;
                    howToPlay.GetComponent<MainMenuItem>().Switch();
                    start.GetComponent<MainMenuItem>().Switch();
                }
                else if (currentOption == options.Credits)
                {
                    currentOption = options.HowToPlay;
                    credits.GetComponent<MainMenuItem>().Switch();
                    howToPlay.GetComponent<MainMenuItem>().Switch();
                }
                else
                {
                    currentOption = options.Credits;
                    quit.GetComponent<MainMenuItem>().Switch();
                    credits.GetComponent<MainMenuItem>().Switch();
                }
            }
            else
            {
                if (currentOption == options.Start)
                {
                    currentOption = options.HowToPlay;
                    start.GetComponent<MainMenuItem>().Switch();
                    howToPlay.GetComponent<MainMenuItem>().Switch();
                }
                else if (currentOption == options.HowToPlay)
                {
                    currentOption = options.Credits;
                    howToPlay.GetComponent<MainMenuItem>().Switch();
                    credits.GetComponent<MainMenuItem>().Switch();
                }
                else if (currentOption == options.Credits)
                {
                    currentOption = options.Quit;
                    credits.GetComponent<MainMenuItem>().Switch();
                    quit.GetComponent<MainMenuItem>().Switch();
                }
                else
                {
                    currentOption = options.Start;
                    quit.GetComponent<MainMenuItem>().Switch();
                    start.GetComponent<MainMenuItem>().Switch();
                }
            }
        }
    }

    void Select()
    {
        selected = true;
        mainMenu.GetComponent<CameraUpAndDown1>().Switch();
        if (currentOption == options.Start)
        {
            selectingPlayers = true;
            logo.GetComponent<CameraUpAndDown1>().Switch();
            playerScreen.SetActive(true);
            Instantiate(controllerSetup); 
        }
        else if(currentOption == options.Credits)
        {
            creditsScreen.GetComponent<CameraUpAndDown1>().Switch();
        } else if(currentOption == options.HowToPlay)
        {
            logo.GetComponent<CameraUpAndDown1>().Switch();
            howToPlayScreen.GetComponent<CameraUpAndDown1>().Switch();
        }
        else
        {
            Application.Quit();
        }
    }

    void Unselect()
    {
        if (currentOption == options.Start)
        {
            /*
            logo.GetComponent<CameraUpAndDown1>().Switch();
            playerScreen.SetActive(false);
            */
        }
        else if (currentOption == options.Credits)
        {
            selected = false;
            selectingPlayers = false;
            creditsScreen.GetComponent<CameraUpAndDown1>().Switch();
            mainMenu.GetComponent<CameraUpAndDown1>().Switch();
        }
        else if (currentOption == options.HowToPlay)
        {
            selected = false;
            selectingPlayers = false;
            logo.GetComponent<CameraUpAndDown1>().Switch();
            howToPlayScreen.GetComponent<CameraUpAndDown1>().Switch();
            mainMenu.GetComponent<CameraUpAndDown1>().Switch();
        }
        
    }
}
