﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuItem : MonoBehaviour {

    public bool selected = false;
    public Sprite select;
    public Sprite unselect;

	// Use this for initialization
	void Start () {
	}
	
    public void Switch()
    {
        if (selected)
        {
            selected = false;
            GetComponent<SpriteRenderer>().sprite = unselect;
        }
        else
        {
            selected = true;
            GetComponent<SpriteRenderer>().sprite = select;
        }
    }
}
