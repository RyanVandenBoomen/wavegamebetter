﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//written by Ryan Vanden Boomen
//
//Controls the camera throughout the pre-game loading phase

public class MoveCamera : MonoBehaviour {

    public Vector3 start;
    public Vector3 target;
    public float time;
    public Transform camera;
    public float count;
    public float shkMag;
    public float shkLng;

	// Use this for initialization
	void Start () {
        camera = GetComponent<Transform>();
        start = camera.position;
	}
	
	// Update is called once per frame
	void Update () {
        MoveToTarget(start, target);
	}

    void MoveToTarget(Vector3 begin, Vector3 end)
    {
        count += Time.deltaTime;
        if (count < time)
        {
            float ratio = count / time;
            camera.position = Vector3.Lerp(begin, end, ratio);
        }
    }
    public void MoveReturn()
    {
        count = 0;
        Vector3 temp = start;
        start = target;
        target = temp;
    }
    public void ScreenShake()
    {
        iTween.ShakePosition(this.gameObject, new Vector3(1 * shkMag, 1 * shkMag, 0), shkLng);
    }
}
