﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//written by Ryan Vanden Boomen
//
//Controls music source and music clips

public class MusicHandler : MonoBehaviour {

    public AudioSource source;
    public AudioClip[] tracks;
    int index = 0;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        source.clip = tracks[index];
        source.Play();
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		if(!source.isPlaying)
        {
            index++;
            if(index>tracks.Length-1)
            {
                index = 0;
            }
            source.clip = tracks[index];
            source.Play();
        }
	}
}
