﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

//written by Ryan Vanden Boomen
//
//Class that contains player attributes

[System.Serializable]
public class Player
{
        public bool isConnected = false;
        public bool isPlaying = false;
        public PlayerController.players player;
    public int score;
    public void increaseScore()
    {
        score++;
    }
    public int getScore()
    {
        return score;
    }
}
