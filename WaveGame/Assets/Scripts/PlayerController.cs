using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//written by Ryan Vanden Boomen
//
//Main script that controls movement, wave push, and spawning mechanics

public class PlayerController : MonoBehaviour {

    public enum players
    { 
        none,
        player1,
        player2,
        player3,
        player4
    }

    public players curPlayer;
    public float dist;
    public float powerMin;
    public float powerMax;
    public float powerAccel;
    public float hitPowerMin;
    public float hitPowerMax;
    public float hitPowerAccel;
    public float waveInterval;
    public string horizWalk;
    public string horizAxis;
    public string vertAxis;
    public string waveShoot;
    public Rigidbody rb;
    public float maxSpeed;
    public float accel;
    public float airSpeed;
    public float slowDownSpeed;
    public float frictionSpeed;
    public float fastFall;
    public float gravSpeedRange;
    public float chargeRatio;

    private float calcAngle;
    private int counter = 0;
    private float power;
    private float hitPower;
    private bool invincible = true;
    public float iTime;
    private float iCount = 0;
    public bool shotIsDown;
    public float holdMax;
    private float holdCount;
    private bool holdLock = false;

    public Vector3 curVel;
    public AudioSource audio;
    public SpawnCollector spawnSet;
    public GameMeta meta;

    public bool freeze;

    public AnimHand anim;

    public ParticleSystem waver;

	// Use this for initialization
	void Start () {
        power = powerMin;
        hitPower = hitPowerMin;
        waver = GetComponentInChildren<ParticleSystem>();
        meta = GameObject.Find("GameMeta").GetComponent<GameMeta>();
        rb = GetComponent<Rigidbody>();
        spawnSet = GameObject.FindGameObjectWithTag("Spawner").GetComponent<SpawnCollector>();
        audio = GetComponent<AudioSource>();
    }



    void Update()
    {
        if (anim == null)
        {
            anim = GetComponentInChildren<AnimHand>();
        }
        float fps = 1.0f / Time.deltaTime;
        if (freeze)
        {
            anim.FalseMove();
        }
        InvCounter();
        ExposeChargeRatio();
    }

    void ExposeChargeRatio()
    {
        chargeRatio = power / powerMax;
    }

	// Update is called once per frame
	void FixedUpdate () {

        if (!freeze)
        {
            ShootWave();
            HorizMovement();
            AddGravity();
        }
        curVel = rb.velocity;
        
	}

    void InvCounter()
    {
        iCount += Time.deltaTime;
        if(iCount >= iTime)
        {
            invincible = false;
        }
    }

    void AddGravity()
    {
        if (!isGrounded() && rb.velocity.y <= gravSpeedRange)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y - fastFall * Time.deltaTime);
        }
    }

    public float GetAngle()
    {
        float x = Input.GetAxis(horizAxis);
        float y = -Input.GetAxis(vertAxis);
        if (x != 0.0f || y != 0.0f)
        {
            return (Mathf.Atan2(y, x) * Mathf.Rad2Deg);
            // Do something with the angle here.
        }
        return 0;
    }

    void FireWave(float waitTime)
    {
            shotIsDown = true;
            Vector3 direction;
            RaycastHit hitCheck;
            int nbrOfRays = 9;

            float curAngle = (calcAngle - 22.5f);

            //ensures that magnitude of ball hitting is independent of rays it catches
            bool canHitBall = true;
            audio.Play();
            waver.transform.rotation = Quaternion.Euler(new Vector3(waver.transform.rotation.x, waver.transform.rotation.y, calcAngle - 90));
            waver.Stop();
            waver.Play();

            for (int i = 0; i < nbrOfRays; i++)
            {
                direction = new Vector3(Mathf.Cos(Mathf.Deg2Rad * curAngle), Mathf.Sin(Mathf.Deg2Rad * curAngle), 0);
                bool rayCast = Physics.Raycast(rb.transform.position, direction, out hitCheck, dist);
                Debug.DrawRay(rb.transform.position, direction * dist, Color.green, 1);

                

                if (rayCast)
                {
                    Vector3 shotDirection = -new Vector3(Mathf.Cos(Mathf.Deg2Rad * calcAngle), Mathf.Sin(Mathf.Deg2Rad * calcAngle));
                    if (hitCheck.transform.gameObject.layer == LayerMask.NameToLayer("Wall"))
                    {
                        if (Mathf.Abs(shotDirection.x) <= 0.2)
                        {
                            rb.velocity = new Vector3(rb.velocity.x, 0, 0);
                        }
                        else if (Mathf.Abs(shotDirection.y) <= 0.2)
                        {
                            rb.velocity = new Vector3(0, rb.velocity.y, 0);
                        }
                        else
                        {
                            rb.velocity = Vector3.zero;
                        }
                        rb.AddForce(power * shotDirection, 0);

                    }
                    //hitting an object, need to modify, experimenting for now
                    if (hitCheck.transform.gameObject.layer == LayerMask.NameToLayer("Ball") && canHitBall)
                    {

                        Rigidbody oRb = hitCheck.transform.gameObject.GetComponent<Rigidbody>();
                        oRb.velocity = Vector3.zero;
                        oRb.AddForce(hitPower * -shotDirection, 0);
                        canHitBall = false;
                        ActivateBall(oRb.gameObject.GetComponent<DodgeBallController>());
                    }
                    if (hitCheck.transform.gameObject.layer == LayerMask.NameToLayer("Player"))
                    {
                        Debug.Log("It works");
                        Rigidbody oRb = hitCheck.transform.gameObject.GetComponent<Rigidbody>();
                        oRb.velocity = Vector3.zero;
                        oRb.AddForce(power * -shotDirection, 0);
                    }
                    counter = 0;

                }
                curAngle += 5;
            }
        power = powerMin;
        hitPower = hitPowerMin;
    }

    void ShootWave()
    {
        if(shotIsDown)
        {
            power = 0;
        }
        float waitTime = waveInterval / Time.deltaTime;
        if ((Mathf.Abs(Input.GetAxis(horizAxis)) < .5 && Mathf.Abs(Input.GetAxis(vertAxis)) < .5) && counter > waitTime && !shotIsDown) {
            FireWave(waitTime);
        }
        else
        {
            counter++;
        }
        if(Mathf.Abs(Input.GetAxis(horizAxis)) >= .5 || Mathf.Abs(Input.GetAxis(vertAxis)) >= .5)
        {
            holdCount += Time.deltaTime;

            if (holdCount > holdMax && !holdLock)
            {
                FireWave(waitTime);
                holdCount = 0;
                holdLock = true;
            }
            else
            {
                calcAngle = GetAngle();
                if (!holdLock)
                {
                    shotIsDown = false;
                }
                if (power < powerMax)
                {
                    power += powerAccel * Time.deltaTime;
                }
                if (hitPower < hitPowerMax)
                {
                    hitPower += hitPowerAccel * Time.deltaTime;
                }
            }
        }
        else
        {
            holdLock = false;
            holdCount = 0;
        }
    }

    void HorizMovement()
    {
        if(rb.velocity.x<0)
        {
            GameObject.Find(this.name+"/Player_Sprite").GetComponent<SpriteRenderer>().flipX = true;
        }
        else if(rb.velocity.x>0)
        {
            GameObject.Find(this.name + "/Player_Sprite").GetComponent<SpriteRenderer>().flipX = false;
        }
        float input = Input.GetAxis(horizWalk);
        if (input < 0)
        {
            GameObject.Find(this.name + "/Player_Sprite").GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (input > 0)
        {
            GameObject.Find(this.name + "/Player_Sprite").GetComponent<SpriteRenderer>().flipX = false;
        }
        if (isGrounded()) { 

           if(Mathf.Abs(input)>=0.1f)
            {
                anim.TrueMove();
                if (Mathf.Abs(rb.velocity.x) > maxSpeed)
                {
                    rb.velocity = new Vector3(Mathf.MoveTowards(rb.velocity.x, maxSpeed, slowDownSpeed*Time.deltaTime), rb.velocity.y, 0);
                }
                else
                {
                    rb.velocity = new Vector3(rb.velocity.x + accel * input * Time.deltaTime, rb.velocity.y, 0);
                }
            }
           else
            {
                anim.FalseMove();
                rb.velocity = new Vector3(Mathf.MoveTowards(rb.velocity.x, 0, frictionSpeed * Time.deltaTime), rb.velocity.y, 0);
            }
           
        }
        else
        {
            anim.FalseMove();
            if (Mathf.Abs(input) >= 0.1f)
            {
                rb.AddForce(airSpeed*input, 0, 0);
            }
        }
    }

    bool isGrounded()
    {


        float castBuffer = -1f;
        bool grounded = false;
        for (int i = 0; i < 3; i++)
        {
            grounded = Physics.Raycast(new Vector3(rb.transform.position.x + castBuffer, rb.transform.position.y, 0), new Vector3(0, -1, 0), 1.2f);
            if (grounded)
            {
                return true;
            }
            castBuffer += 0.49f;
        }
        return false;
    }

    public void KillPlayer()
    {
        meta.KillSound();
        GameObject.Find("GameCamera").GetComponent<MoveCamera>().ScreenShake();
        Destroy(this.gameObject);
        spawnSet.spawners[Random.Range(0, 2)].GetComponent<PlayerSpawn>().SpawnPlayer(curPlayer);
        //use instantiation method in spawning system
    }


    void ActivateBall(DodgeBallController ball)
    {
        ball.ActivateBall(curPlayer);
    }

    void OnCollisionEnter(Collision c)
    {
        
        if (c.gameObject.tag == "Platform")
        {
            Debug.Log(c.contacts[0].normal);
            Vector3 normal = c.contacts[0].normal;
            if (normal.x != 0)
            {
                rb.AddForce(new Vector3(Mathf.Sign(c.contacts[0].normal.x) * 100, 0, 0));
            }
        }
        if (c.gameObject.tag == "Ball")
        {
            PlayerController.players oppPlay = c.gameObject.GetComponent<DodgeBallController>().currentPlayer;
            if (oppPlay != curPlayer && c.gameObject.GetComponent<DodgeBallController>().currentPlayer != players.none && !invincible)
            {
                meta.AddScore(oppPlay);
                KillPlayer();
            }
            else
            {
                c.gameObject.GetComponent<DodgeBallController>().ActivateBall(curPlayer);
            }
        }


    }

}
