﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScreenShit : MonoBehaviour {

    public GameObject p1;
    public GameObject p2;
    public GameObject p3;
    public GameObject p4;
    public GameObject meta;
    public GameObject main;
    public GameObject pressStart;

    // Use this for initialization
    void Start()
    {

    }
	// Update is called once per frame
	void Update () {
        FindPlayers();
        if (!main.GetComponent<MainMenu>().selectingPlayers)
        {
            if (p1.GetComponent<MainMenuItem>().selected)
            {
                p1.GetComponent<MainMenuItem>().Switch();
            }
            if (p2.GetComponent<MainMenuItem>().selected)
            {
                p2.GetComponent<MainMenuItem>().Switch();
            }
            if (p3.GetComponent<MainMenuItem>().selected)
            {
                p3.GetComponent<MainMenuItem>().Switch();
            }
            if (p4.GetComponent<MainMenuItem>().selected)
            {
                p4.GetComponent<MainMenuItem>().Switch();
            }
        }
        if (meta.GetComponent<GameMeta>().playset[0].isPlaying && !p1.GetComponent<MainMenuItem>().selected)
        {
            p1.GetComponent<MainMenuItem>().Switch();
        }
        if (meta.GetComponent<GameMeta>().playset[1].isPlaying && !p2.GetComponent<MainMenuItem>().selected)
        {
            p2.GetComponent<MainMenuItem>().Switch();
        }
        if (meta.GetComponent<GameMeta>().playset[2].isPlaying && !p3.GetComponent<MainMenuItem>().selected)
        {
            p3.GetComponent<MainMenuItem>().Switch();
        }
        if (meta.GetComponent<GameMeta>().playset[3].isPlaying && !p4.GetComponent<MainMenuItem>().selected)
        {
            p4.GetComponent<MainMenuItem>().Switch();
        }
        if (!meta.GetComponent<GameMeta>().playset[0].isPlaying && p1.GetComponent<MainMenuItem>().selected)
        {
            p1.GetComponent<MainMenuItem>().Switch();
        }
        if (!meta.GetComponent<GameMeta>().playset[1].isPlaying && p2.GetComponent<MainMenuItem>().selected)
        {
            p2.GetComponent<MainMenuItem>().Switch();
        }
        if (!meta.GetComponent<GameMeta>().playset[2].isPlaying && p3.GetComponent<MainMenuItem>().selected)
        {
            p3.GetComponent<MainMenuItem>().Switch();
        }
        if (!meta.GetComponent<GameMeta>().playset[3].isPlaying && p4.GetComponent<MainMenuItem>().selected)
        {
            p4.GetComponent<MainMenuItem>().Switch();
        }
    }

    void FindPlayers()
    {
        int count = 0;
        for(int i = 0; i < meta.GetComponent<GameMeta>().playset.Length; i++)
        {
            if(meta.GetComponent<GameMeta>().playset[i].isPlaying)
            {
                count++;
            }
        }
        if(count>1)
        {
            pressStart.SetActive(true);
        }
        else
        {
            pressStart.SetActive(false);
        }
    }
}
