﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//written by Ryan Vanden Boomen
//
//Controls player spawning

public class PlayerSpawn : MonoBehaviour {

    public GameObject playerOne;
    public GameObject playerTwo;
    public GameObject playerThree;
    public GameObject playerFour;
    public GameObject ball;

    public Vector3 launchVector;
    public float launchSpeed;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SpawnBall()
    {
        GameObject newBall = Instantiate(ball, GetComponent<Transform>().position, Quaternion.identity);
        newBall.GetComponent<Rigidbody>().AddForce(launchVector * launchSpeed);
    }

    public void SpawnPlayer(PlayerController.players player)
    {
        if(player == PlayerController.players.player1)
        {
            Instantiate(playerOne, GetComponent<Transform>().position, Quaternion.identity);
        }
        if (player == PlayerController.players.player2)
        {
            Instantiate(playerTwo, GetComponent<Transform>().position, Quaternion.identity);
        }
        if (player == PlayerController.players.player3)
        {
            Instantiate(playerThree, GetComponent<Transform>().position, Quaternion.identity);
        }
        if (player == PlayerController.players.player4)
        {
            Instantiate(playerFour, GetComponent<Transform>().position, Quaternion.identity);
        }
    }
}
