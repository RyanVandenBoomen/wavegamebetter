﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoring : MonoBehaviour {

    public int playerOneStock;
    public int playerTwoStock;
    public int stock;

	// Use this for initialization
	void Start () {
        playerOneStock = stock;
        playerTwoStock = stock;
	}

    void PlayerLoseStock(int player)
    {
        if(player == 1)
        {
            if (playerOneStock > 0)
            {
                playerOneStock--;
            }
        }
        else if (player == 2)
        {
            if (playerTwoStock > 0)
            {
                playerTwoStock--;
            }
        } else { Debug.Log("Nonvalid player number"); }
    }

    int CheckWinner()
    {
        if (playerOneStock <= 0) { return 2; }
        else if (playerTwoStock <= 0) { return 1; }
        else { return 0; }
    }

    void ChangeStock(int newStock)
    {
        playerOneStock += newStock - stock;
        playerTwoStock += newStock - stock;
        stock = newStock;
    }

    void Restart()
    {
        playerOneStock = stock;
        playerTwoStock = stock;
    }
}
