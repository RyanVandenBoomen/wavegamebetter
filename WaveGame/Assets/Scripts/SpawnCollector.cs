﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//written by Ryan Vanden Boomen
//
//Container for player spawners

public class SpawnCollector : MonoBehaviour {

    public PlayerSpawn[] spawners;

	// Use this for initialization
	void Start () {
        spawners = GetComponentsInChildren<PlayerSpawn>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
